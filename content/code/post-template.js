window.onload = jQuery("span").each(function() {
    function leftPad(num) {
        if (num < 10) {
        return '0' + num
        }
        return num
    }
    var d = new Date()
    var year = d.getFullYear()
    var month = leftPad(d.getMonth() + 1)
    var day = leftPad(d.getDate())
    var hour = leftPad(d.getHours())
    var min = leftPad(d.getMinutes())
    var sec = leftPad(d.getSeconds())
    var tz = d.getTimezoneOffset()/60
    timeStr = year + '-' + month + '-' + day + 'T' + hour + ':' + min + ':' + sec
    if (jQuery(this).html() == "2019-02-24T10:21:43") {
        jQuery(this).html(timeStr)
        jQuery(this).next().html(leftPad(Math.abs(tz)))
    }
})
