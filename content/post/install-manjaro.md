---
title: Manjaro Install Notes
date: 2018-04-01T12:27:28+08:00
lastmod: 2019-02-24T10:14:00+08:00
draft: false
tags: ["linux", "code"]
---

## Packages

``` shell
$ sudo pacman -S chromium mpv gvim  \
ttf-roboto ttf-droid adobe-source-code-pro-fonts \
adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts \
ibus-libpinyin \
virtualbox virtualbox-host-dkms \
tree

$ yaourt passwordsafe
```

### yaourt

change /etc/yaourtrc:

`AURURL="https://aur.tuna.tsinghua.edu.cn"`

### fonts

> monospace

ttf-roboto ttf-droid adobe-source-code-pro-fonts

> chinese

adobe-source-han-sans-cn-fonts adobe-source-han-serif-cn-fonts

### input method

ibus-googlepinyin (谷歌拼音, ibus),
ibus-libpinyin (newer)
ibus

start ibus daemon:
`ibus-daemon -r -d`

### virtualbox [#](https://wiki.archlinux.org/index.php/VirtualBox)

virtualbox virtualbox-host-dkms
> 需要重启

### xfce

> 任务栏时间格式

`%m.%d %a %T`

### bash

[pkgfile](https://wiki.archlinux.org/index.php/Pkgfile)
> 自动检测包含命令的包

### chromium

plugins

* [SwitchyOmega](https://github.com/FelisCatus/SwitchyOmega)
* [crx-selection-translate](https://github.com/Selection-Translator/crx-selection-translate)
* [SmoothScroll](https://github.com/galambalazs/smoothscroll/)
* [ReplaceGoogleCDN](https://github.com/justjavac/ReplaceGoogleCDN)
* [Reddit Enhancement Suite](https://github.com/honestbleeps/Reddit-Enhancement-Suite)

### zsh [#](https://github.com/robbyrussell/oh-my-zsh)

`$ chsh -s /bin/zsh`

then install zsh according to github

## Fix chrome screen tear [#](https://wiki.manjaro.org/index.php?title=Using_Compton_for_a_tear-free_experience_in_Xfce)

summary:

```bash
$ sudo pacman -S compton
$ xfconf-query -c xfwm4 -p /general/use_compositing -s false
$ cat ~/.config/autostart/compton.desktop
[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=Compton
Comment=X11 compositor
Exec=compton -b
OnlyShowIn=XFCE;
StartupNotify=false
Terminal=false
Hidden=false
```

### Usefull commands

> converts file name encoding

`pacman -S convmv; convmv`

## Tricks

### script that tells me to relax

`30 * * * * env DISPLAY=:0.0 /home/rusher/workspace/venv3/bin/python /home/rusher/bin/relax.py`

## Network

### DNS

8.8.8.8, 8.8.4.4, 223.5.5.5, 223.6.6.6
